#APP 下载地址管理

## 1.鸿博伟业APP


#### HBCAM
IOS：
> https://itunes.apple.com/app/id974661353

ANDROID：
>国内：   
>http://apps.wandoujia.com/apps/com.android.thcam/download   
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.bhbcam

#### CFL-FPV
IOS:
>https://itunes.apple.com/app/id1002973780

ANDROID：
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3299241?recrefer=SE_D_CFL-FPV   
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.ufo.hb



#### WIFI.L-FPV
IOS:
>https://itunes.apple.com/app/id1090715477

Android:
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3577019?recrefer=SE_D_WIFI.L-FPV   
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.ufo.wifil_fpv


#### WIFIDVR
IOS:
>https://itunes.apple.com/app/id1107515677

ANDROID：
>国内：http://www.wandoujia.com/apps/com.hbwy.plugplay   
>  
>国外：https://play.google.com/store/apps/details?id=com.hbwy.plugplay
>

#### IPLiveCam
IOS:
>https://itunes.apple.com/app/id1193123768

ANDROID：
>国内：   
>http://m.app.so.com/detail/index?pname=com.hbwy.fan.iminicams&id=3646142  
>国外：  
>https://play.google.com/store/apps/details?id=com.hbwy.fan.iminicams

## 1.星光威 APP

#### BumbleBEE Campro
IOS:
>https://itunes.apple.com/app/id1190014163

Android:
>国内：   
>http://m.app.so.com/detail/index?pname=com.cfans.ufo.BumbleBEE&id=3619528   
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.ufo.BumbleBEE

#### XGW-CX10
IOS:
>https://itunes.apple.com/app/id1203283767

Android:
>国内：   
>http://m.app.so.com/detail/index?pname=com.cfans.ufo.xgw_cx10&id=3687790
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.ufo.xgw_cx10

#### Q Pro FPV
IOS:
>https://itunes.apple.com/app/id1214625695

Android:
>国内：   
>http://m.app.so.com/detail/index?pname=com.cfans.ufo.qprofpv&id=3745510
>国外：  
>

## 1.重庆腾隆光电（望远镜） APP

#### Telescope
IOS:
>https://itunes.apple.com/app/id1239007402

Android:
>国内：   
>http://m.app.so.com/detail/index?pname=com.cfans.ufo.telescope&id=3845679   
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.ufo.telescope

## 1.鼎点 APP

#### kingdowin
IOS:
>https://itunes.apple.com/app/id1238120468

Android:
>国内：   
>http://m.app.so.com/detail/index?pname=com.cfans.ufo.dian&id=3842992   
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.ufo.dian


##  2.时代杰诚APP

#### XBM-FPV
IOS: 
Bundle ID：com.cfans.ufo.xbm
>https://itunes.apple.com/app/id1072356010
ANDROID:

>包名：com.cfans.ufo.xbm   
>国内：     
>http://apps.wandoujia.com/apps/com.cfans.ufo.xbm/download      
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.xbm&hl=zh-CN

#### 伟力：
ANDROID：
>国内：   
>http://www.wandoujia.com/apps/com.cfans.ufo.weili   
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.weili&hl=zh-CN

IOS：
>https://itunes.apple.com/app/id1076077926


#### GPTOYS
IOS:
>https://itunes.apple.com/app/id1090368375

Android:
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.gptoys&hl=zh-CN   
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3230071?recrefer=SE_D_GPTOYS


#### BJ-FPV
IOS:
>https://itunes.apple.com/app/id1090317607

Android:
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.bj&hl=zh-CN   
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3230078?recrefer=SE_D_BJ-FPV

#### EXPLORATION-FPV
IOS:
>https://itunes.apple.com/app/id1092813126

#### Helicute-UFO
IOS:
>https://itunes.apple.com/app/id1090369565

Anroid:
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.helicute&hl=zh-CN   
>国内：   
http://zhushou.360.cn/detail/index/soft_id/3232072?recrefer=SE_D_Helicute-UFO


#### Wifi.J-Aircraft
IOS:
>https://itunes.apple.com/app/id1090713428

Android:
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.sdj&hl=zh-CN   
>国内:   
>http://zhushou.360.cn/detail/index/soft_id/3232071?recrefer=SE_D_Wifi.J-Aircraft

#### Wifi.L-Aircraft
IOS:
>https://itunes.apple.com/app/id1076840927

Android:
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.sdl&hl=en   
>国内:      
>http://zhushou.360.cn/detail/index/soft_id/3225843?recrefer=SE_D_Wifi.J-Aircraft

#### XINXUN-FPV
IOS:
>https://itunes.apple.com/app/id1092823647

Android:
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.xinxun&hl=zh-CN   
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3231085?recrefer=SE_D_XINXUN-FPV


#### BAYANGTOYS
IOS:
>https://itunes.apple.com/app/id1090368280

Android:
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3234548?recrefer=SE_D_bayangtoys   
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.sdw17&hl=en

#### HX-FPV
IOS:
>https://itunes.apple.com/app/id1104646332

Android:
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3260532?recrefer=SE_D_HX-UFO   


#### Metakoo
IOS:
>https://itunes.apple.com/app/id1112803650

Android:
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.metakoo&hl=en

#### KHILONA
IOS:
>https://itunes.apple.com/app/id1126179097

Android:
>国外：   
>https://play.google.com/store/apps/details?id=com.cfans.ufo.jx&hl=en   
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3279052?recrefer=SE_D_KHILONA

##  3.张峰客户APP

#### BOBOView
IOS:
>https://itunes.apple.com/app/id1114481366

## 4.奥启晟APP

#### Soundlogic | XT Lightwave
IOS：
> https://itunes.apple.com/app/id1130150952

ANDROID：  
>国外：  
>   
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3320189?recrefer=SE_D_Soundlogic%20|%20XT%20Lightwave

#### iLIGHTWAVE
IOS：
> https://itunes.apple.com/app/id1166429887

ANDROID：  
>国外：  
>   
>国内：   
>http://m.app.so.com/detail/index?pname=com.hbwy.fan.iLIGHTWAVE&id=3525432

## 5.福建林总720P对应的APP

#### CYCLECAM
IOS：
> https://itunes.apple.com/app/id1133880489

ANDROID：  
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.ufo.simple&hl=zh_CN   
>国内：   
>http://zhushou.360.cn/detail/index/soft_id/3334364?recrefer=SE_D_CYCLECAM

## 6.易天富 APP

#### Droneview-mini
IOS:
>https://itunes.apple.com/app/id1218305165

Android:
>国内：   
>   
>国外：  
>https://play.google.com/store/apps/details?id=com.cfan.ufo.droneview

#### Revell_Spot_VR
IOS:
>https://itunes.apple.com/app/id1219992247

Android:
>国内：   
>   
>国外：  
>https://play.google.com/store/apps/details?id=com.cfans.ufo.revell

