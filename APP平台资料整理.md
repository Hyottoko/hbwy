

### 创作旗号
- 为自由而生

### APP创作源泉
- [阿里云API](https://market.aliyun.com/data?spm=5176.730006-cmapi014122.401001.5.iD0txs)   

- [ASO100](https://aso100.com/rank/float/float/up)
- [DeepASO](http://www.deepaso.com)

### APP发布平台聚合
####  [酷传](http://publish.kuchuan.com/myaccount/accounts) 
- [绿安-cfans(未通过)](http://cp.jxcrtech.com/index#)
- [木蚂蚁-cfans_(身份证格式不对)](http://dev.mumayi.com/user/view)
- [应用汇-13088824490](http://dev.appchina.com/dev/manage/main)
- [乐视应用商店-13088824490](http://open.le.com/dev-web/app/list.html)   
- [联通沃商店-cfans](http://dev.wostore.cn/wo/toIndex)
- [搜狗手机助手-731666148@qq.com](http://zhushou.sogou.com/open/)
- [机锋市场-cfans_](http://dev.gfan.com)
- [锤子应用商店-13088824490](http://dev.smartisan.com)    
- [安智市场-cfans_](http://dev.anzhi.com/remind.php?read_status=2) 
- [爱奇艺平台-13088824490](http://open.iqiyi.com/developer/app/list?state=0)
- [三星应用商店](http://seller.samsungapps.com/main/sellerMain.as)
- 联想开放平台(企业)
- oppo vivo（企业）
- N多网(企业)
- 
#### 国内APP发布平台
- [360平台(不支持个人首发)](http://dev.360.cn/mod3/mobilenavs/index?qid=1364370450)   
- [百度平台(不支持个人首发)](https://app.baidu.com/first/?frompos=720112) 
- [腾讯开发平台](http://op.open.qq.com/manage_centerv2/android?owner=731666148&uin=731666148)
- [阿里应用分发平台](http://open.uc.cn) 
- [华为平台](http://developer.huawei.com/consumer/cn/)
- [小米平台](https://dev.mi.com/console/)   
- [oppo平台(只支持企业)](http://open.oppomobile.com)

#### 打包工具
- [packer-ng-plugin](https://github.com/mcxiaoke/packer-ng-plugin)   
- [Gradle打包](http://stormzhang.com/devtools/2015/01/15/android-studio-tutorial6/)

#### 自动更新模块选择
- [Bmob]( 
http://docs.bmob.cn/data/Android/e_autoupdate/doc/index.html#快速入门)  
- [360](http://dev.360.cn/dev/service)   
- [百度](https://app.baidu.com/value/sdkservice?frompos=200131)   
- [蒲公英-付费](https://www.pgyer.com/sdk)
- [讯飞](http://www.xfyun.cn/services/autoupdate/) 

#### 用户反馈

- [百川](http://baichuan.taobao.com/docs/doc.htm?spm=a3c0d.7629140.0.0.PjMrGw&treeId=118&articleId=105686&docType=1)
- [阿里云](https://www.aliyun.com/product/feedback)

#### 广告平台
- [Google Play的APP用Admob](https://www.google.com/admob/)
- [360](http://dev.360.cn/dev/service)
- [百度](https://app.baidu.com/value/sdkservice?frompos=200131)

http://www.domob.cn/offerwall
http://e.qq.com/ads
https://www.youmi.net
http://union.baidu.com/product/prod-mobile.html
http://china.inmobi.com

参考资料：   
- [app 的 Android 和 iOS 下载量](https://www.zhihu.com/question/28533067)   
-




